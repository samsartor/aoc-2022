use fxhash::{FxHashMap, FxHashSet};
use inpt::{inpt, Inpt};
use std::cmp::Ordering;
pub use std::collections::hash_map::Entry;
use std::collections::*;
use std::hash::Hash;
use std::iter::repeat;
pub use std::mem::{replace, swap};
use std::ops::RangeBounds;

pub type I = i64;
pub type V<T> = Vec<T>;
pub type S<T> = FxHashSet<T>;
pub type M<K, V> = FxHashMap<K, V>;

pub trait StrExt {
    fn read<'s, T: Inpt<'s>>(&'s self) -> T;

    fn at(&self, idx: usize) -> char;
    fn try_at(&self, idx: usize) -> Option<char>;
}

impl StrExt for str {
    fn read<'s, T: Inpt<'s>>(&'s self) -> T {
        inpt(self).unwrap()
    }

    fn at(&self, idx: usize) -> char {
        self[idx..].chars().next().expect("indexed end of string")
    }

    fn try_at(&self, idx: usize) -> Option<char> {
        if idx >= self.len() {
            return None;
        }
        self[idx..].chars().next()
    }
}

pub trait UintExt {
    fn set_bit(self, idx: usize, value: bool) -> Self;
    fn get_bit(self, idx: usize) -> bool;
}

impl UintExt for usize {
    fn set_bit(self, idx: usize, value: bool) -> Self {
        let shifted = 1 << idx;
        match value {
            true => self | shifted,
            false => self & !shifted,
        }
    }

    fn get_bit(self, idx: usize) -> bool {
        (self >> idx) & 1 != 0
    }
}

impl UintExt for u64 {
    fn set_bit(self, idx: usize, value: bool) -> Self {
        let shifted = 1 << idx;
        match value {
            true => self | shifted,
            false => self & !shifted,
        }
    }

    fn get_bit(self, idx: usize) -> bool {
        (self >> idx) & 1 != 0
    }
}

pub trait MapExt<K, V> {
    fn mutate(&mut self, key: K) -> &mut V;
}

impl<K: Hash + Eq, V: Default> MapExt<K, V> for HashMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

impl<K: Ord, V: Default> MapExt<K, V> for BTreeMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

pub trait IterExt<I>: Sized {
    fn count_eq(self, value: I) -> usize
    where
        I: PartialEq;

    fn reduce_all<V>(self, start: V, reduce: impl FnMut(&V, &I) -> V) -> FxHashMap<I, V>
    where
        I: Hash + Eq;

    fn count_all(self) -> FxHashMap<I, usize>
    where
        I: Hash + Eq,
    {
        self.reduce_all(0, |n, _| *n + 1)
    }
}

impl<T: IntoIterator> IterExt<T::Item> for T {
    fn count_eq(self, value: T::Item) -> usize
    where
        T::Item: PartialEq,
    {
        self.into_iter().filter(eq(value)).count()
    }

    fn reduce_all<V>(
        self,
        start: V,
        mut reduce: impl FnMut(&V, &T::Item) -> V,
    ) -> FxHashMap<T::Item, V>
    where
        T::Item: Hash + Eq,
    {
        let mut map = FxHashMap::default();
        for i in self {
            let e = map.entry(i);
            match e {
                Entry::Occupied(mut e) => {
                    *e.get_mut() = reduce(e.get(), e.key());
                }
                Entry::Vacant(e) => {
                    let v = reduce(&start, e.key());
                    e.insert(v);
                }
            }
        }
        map
    }
}

pub trait SliceExt<'s, T> {
    type Perms;
    type Combs<const K: usize>;
    type CombsWithK;

    /// Iterate over all permutations of elements of self.    
    fn perms(self) -> Self::Perms;
    /// Iterate over all combitations of `K` elemnts of self, *with or without
    /// replacement*. `K` is a constant.
    fn combs<const K: usize>(self, replace: bool) -> Self::Combs<K>;
    /// Iterate over all combitations of `K` elemnts of self, *with or without
    /// replacement*. `K` is a constant.
    fn combs_with_k(self, k: usize, replace: bool) -> Self::CombsWithK;
}

impl<'s, T> SliceExt<'s, T> for &'s [T] {
    type Perms = PermsWithNIter<'s, T>;
    type Combs<const K: usize> = CombsIter<'s, K, T>;
    type CombsWithK = CombsWithKIter<'s, T>;

    fn perms(self) -> PermsWithNIter<'s, T> {
        PermsWithNIter(self, EnumeratePerms::new_with_n(self.len()))
    }

    fn combs<const K: usize>(self, replace: bool) -> CombsIter<'s, K, T> {
        CombsIter(self, EnumerateCombs::new(self.len(), replace))
    }

    fn combs_with_k(self, k: usize, replace: bool) -> CombsWithKIter<'s, T> {
        CombsWithKIter(self, EnumerateCombs::new_with_k(self.len(), k, replace))
    }
}

impl<'s, const N: usize, T> SliceExt<'s, T> for &'s [T; N] {
    type Perms = PermsIter<'s, N, T>;
    type Combs<const K: usize> = CombsIter<'s, K, T>;
    type CombsWithK = CombsWithKIter<'s, T>;

    fn perms(self) -> PermsIter<'s, N, T> {
        PermsIter(self, EnumeratePerms::new())
    }

    fn combs<const K: usize>(self, replace: bool) -> CombsIter<'s, K, T> {
        self.as_slice().combs(replace)
    }

    fn combs_with_k(self, k: usize, replace: bool) -> CombsWithKIter<'s, T> {
        self.as_slice().combs_with_k(k, replace)
    }
}

pub struct EnumeratePerms<I> {
    first: bool,
    stack: I,
    inds: I,
    i: usize,
}

impl<const N: usize> EnumeratePerms<[usize; N]> {
    pub fn new() -> Self {
        let mut inds = [0; N];
        for i in 0..N {
            inds[i] = i;
        }

        EnumeratePerms {
            stack: [0; N],
            inds,
            first: true,
            i: 0,
        }
    }
}

impl<I: FromIterator<usize>> EnumeratePerms<I> {
    pub fn new_with_n(n: usize) -> Self {
        EnumeratePerms {
            stack: repeat(0).take(n).collect(),
            inds: (0..n).collect(),
            first: true,
            i: 0,
        }
    }
}

impl<I: AsMut<[usize]>> EnumeratePerms<I> {
    pub fn step(&mut self) -> Option<&I> {
        if replace(&mut self.first, false) {
            return Some(&self.inds);
        }

        let stack = self.stack.as_mut();
        let inds = self.inds.as_mut();
        let n = inds.len();

        loop {
            if self.i >= n {
                return None;
            }

            if stack[self.i] < self.i {
                if self.i % 2 == 0 {
                    inds.swap(0, self.i);
                } else {
                    inds.swap(stack[self.i], self.i);
                }

                stack[self.i] += 1;
                self.i = 0;
                return Some(&self.inds);
            } else {
                stack[self.i] = 0;
                self.i += 1
            }
        }
    }
}

impl<I: AsMut<[usize]> + Clone> Iterator for EnumeratePerms<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.step()?.clone())
    }
}

pub struct EnumerateCombs<I> {
    replace: bool,
    first: bool,
    n: usize,
    inds: I,
}

impl<const K: usize> EnumerateCombs<[usize; K]> {
    pub fn new(n: usize, replace: bool) -> Self {
        assert!(n >= K);
        let mut inds = [0; K];
        if !replace {
            for i in 0..K {
                inds[i] = i;
            }
        }

        EnumerateCombs {
            replace,
            inds,
            n,
            first: true,
        }
    }
}

impl<I: FromIterator<usize>> EnumerateCombs<I> {
    pub fn new_with_k(n: usize, k: usize, replace: bool) -> Self {
        assert!(n >= k);
        EnumerateCombs {
            replace,
            inds: if replace {
                repeat(0).take(k).collect()
            } else {
                (0..k).collect()
            },
            n,
            first: true,
        }
    }
}

impl<I: AsMut<[usize]>> EnumerateCombs<I> {
    pub fn step(&mut self) -> Option<&I> {
        if replace(&mut self.first, false) {
            return Some(&self.inds);
        }

        let inds = self.inds.as_mut();
        let k = inds.len();
        let mut to_inc = None;
        if self.replace {
            for i in (0..k).rev() {
                if inds[i] < self.n - 1 {
                    to_inc = Some(i);
                    break;
                } else {
                    inds[i] = 0;
                }
            }

            let to_inc = to_inc?;
            inds[to_inc] += 1;
        } else {
            for i in (0..k).rev() {
                if inds[i] < self.n - k + i {
                    to_inc = Some(i);
                    break;
                }
            }

            let to_inc = to_inc?;
            let x = inds[to_inc];
            for i in to_inc..k {
                inds[i] = x - to_inc + i + 1;
            }
        }

        Some(&self.inds)
    }
}

impl<I: AsMut<[usize]> + Clone> Iterator for EnumerateCombs<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.step()?.clone())
    }
}

pub struct PermsIter<'s, const N: usize, T: 's>(&'s [T; N], EnumeratePerms<[usize; N]>);
impl<'s, const N: usize, T: 's> Iterator for PermsIter<'s, N, T> {
    type Item = [&'s T; N];

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.1.step()?.map(|ind| &self.0[ind]))
    }
}

pub struct PermsWithNIter<'s, T: 's>(&'s [T], EnumeratePerms<Vec<usize>>);
impl<'s, T: 's> Iterator for PermsWithNIter<'s, T> {
    type Item = Vec<&'s T>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.1.step()?.iter().map(|ind| &self.0[*ind]).collect())
    }
}

pub struct CombsIter<'s, const K: usize, T: 's>(&'s [T], EnumerateCombs<[usize; K]>);
impl<'s, const K: usize, T: 's> Iterator for CombsIter<'s, K, T> {
    type Item = [&'s T; K];

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.1.step()?.map(|ind| &self.0[ind]))
    }
}

pub struct CombsWithKIter<'s, T: 's>(&'s [T], EnumerateCombs<Vec<usize>>);
impl<'s, T: 's> Iterator for CombsWithKIter<'s, T> {
    type Item = Vec<&'s T>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.1.step()?.iter().map(|ind| &self.0[*ind]).collect())
    }
}

#[test]
fn test_combs() {
    assert_eq!(
        EnumerateCombs::<[usize; 3]>::new(5, false).collect::<Vec<_>>(),
        vec![
            [0, 1, 2],
            [0, 1, 3],
            [0, 1, 4],
            [0, 2, 3],
            [0, 2, 4],
            [0, 3, 4],
            [1, 2, 3],
            [1, 2, 4],
            [1, 3, 4],
            [2, 3, 4],
        ]
    )
}

#[test]
fn test_replaced_combs() {
    assert_eq!(
        EnumerateCombs::<[usize; 2]>::new(3, true).collect::<Vec<_>>(),
        vec![
            [0, 0],
            [0, 1],
            [0, 2],
            [1, 0],
            [1, 1],
            [1, 2],
            [2, 0],
            [2, 1],
            [2, 2]
        ]
    )
}

pub fn eq<T: PartialEq>(value: T) -> impl Fn(&T) -> bool {
    move |x| value.eq(x)
}

#[inline]
fn cmp_bounds<T: PartialOrd>(
    a: Bound<&T>,
    b: Bound<&T>,
    a_side: Ordering,
    b_side: Ordering,
) -> Ordering {
    use Bound::*;
    use Ordering::*;
    match (a, b) {
        (Included(i) | Excluded(i), Included(j) | Excluded(j)) => match i.partial_cmp(j) {
            None => a_side,
            Some(Less) => Less,
            Some(Greater) => Greater,
            Some(Equal) => match (a, b) {
                (Included(_), Included(_)) => Equal,
                (Included(_), Excluded(_)) => b_side,
                (Excluded(_), Included(_)) => a_side.reverse(),
                (Excluded(_), Excluded(_)) => Equal,
                _ => unreachable!(),
            },
        },
        (Unbounded, Unbounded) => Equal,
        (Unbounded, _) => a_side,
        (_, Unbounded) => b_side.reverse(),
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum RangeRelation {
    BeforeStart,
    IntersectsStart,
    Contains,
    Same,
    Contained,
    IntersectsEnd,
    AfterEnd,
}

impl RangeRelation {
    #[inline]
    pub fn from_bounds<T: PartialOrd>(
        start1: Bound<&T>,
        end1: Bound<&T>,
        start2: Bound<&T>,
        end2: Bound<&T>,
    ) -> Self {
        use Ordering::*;
        use RangeRelation::*;
        match (
            cmp_bounds(end1, start2, Greater, Less),
            cmp_bounds(start1, start2, Less, Less),
            cmp_bounds(end1, end2, Greater, Greater),
            cmp_bounds(start1, end2, Less, Greater),
        ) {
            (_, Equal, Equal, _) => Same,
            (_, Less | Equal, Greater | Equal, _) => Contains,
            (_, Greater | Equal, Less | Equal, _) => Contained,
            (Greater | Equal, Less, _, _) => IntersectsStart,
            (_, _, Greater, Less | Equal) => IntersectsEnd,
            (Less, Less, _, _) => BeforeStart,
            (_, _, Greater, Greater) => AfterEnd,
        }
    }

    pub fn reverse(self) -> Self {
        use RangeRelation::*;
        match self {
            BeforeStart => AfterEnd,
            IntersectsStart => IntersectsEnd,
            Contains => Contained,
            Same => Same,
            Contained => Contains,
            IntersectsEnd => IntersectsStart,
            AfterEnd => BeforeStart,
        }
    }

    #[inline]
    pub fn overlaps(self) -> bool {
        use RangeRelation::*;
        match self {
            BeforeStart | AfterEnd => false,
            _ => true,
        }
    }

    #[inline]
    pub fn disjoint(self) -> bool {
        use RangeRelation::*;
        match self {
            BeforeStart | AfterEnd => true,
            _ => false,
        }
    }

    #[inline]
    pub fn contains(self) -> bool {
        use RangeRelation::*;
        match self {
            Contains | Same => true,
            _ => false,
        }
    }

    #[inline]
    pub fn redundant(self) -> bool {
        use RangeRelation::*;
        match self {
            Contains | Same | Contained => true,
            _ => false,
        }
    }
}

pub trait RangeExt<O, T>: RangeBounds<T> {
    fn rel(&self, other: &O) -> RangeRelation;

    fn overlaps(&self, other: &O) -> bool {
        self.rel(other).overlaps()
    }
    fn disjoint(&self, other: &O) -> bool {
        self.rel(other).disjoint()
    }
    fn contains(&self, other: &O) -> bool {
        self.rel(other).contains()
    }
    fn redundant(&self, other: &O) -> bool {
        self.rel(other).redundant()
    }
}

impl<T, A, B> RangeExt<B, T> for A
where
    A: RangeBounds<T>,
    B: RangeBounds<T>,
    T: PartialOrd,
{
    fn rel(&self, other: &B) -> RangeRelation {
        RangeRelation::from_bounds(
            self.start_bound(),
            self.end_bound(),
            other.start_bound(),
            other.end_bound(),
        )
    }
}

#[test]
fn test_unbounded_range_rel() {
    use RangeRelation::*;
    assert_eq!((..).rel(&(-10..10)), Contains);
    assert_eq!((..).rel(&(-10..)), Contains);
    assert_eq!((..).rel(&(..10)), Contains);
    assert_eq!((-10..10).rel(&(..)), Contained);
    assert_eq!((-10..).rel(&(..)), Contained);
    assert_eq!((..10).rel(&(..)), Contained);
    assert_eq!(RangeExt::<_, i32>::rel(&(..), &(..)), Same);
    assert_eq!((..-10).rel(&(10..)), BeforeStart);
    assert_eq!((..10).rel(&(-10..)), IntersectsStart);
    assert_eq!((10..).rel(&(..-10)), AfterEnd);
    assert_eq!((-10..).rel(&(..10)), IntersectsEnd);
}

#[test]
fn test_inclusive_range_rel() {
    use RangeRelation::*;

    assert_eq!(RangeExt::rel(&(-100..=-20), &(-10..=10)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-100..=-10), &(-10..=10)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-100..=0), &(-10..=10)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-100..=10), &(-10..=10)), Contains);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-10..=10)), Same);
    assert_eq!(RangeExt::rel(&(-10..=100), &(-10..=10)), Contains);
    assert_eq!(RangeExt::rel(&(0..=100), &(-10..=10)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(10..=100), &(-10..=10)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(20..=100), &(-10..=10)), AfterEnd);

    assert_eq!(RangeExt::rel(&(-10..=10), &(20..=100)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-10..=10), &(10..=100)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-10..=10), &(0..=100)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-10..=100)), Contained);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-10..=10)), Same);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-100..=10)), Contained);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-100..=0)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-100..=-10)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(-10..=10), &(-100..=-20)), AfterEnd);
}

#[test]
fn test_exclusive_range_rel() {
    use RangeRelation::*;

    assert_eq!(RangeExt::rel(&(-100..-20), &(-10..10)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-100..-10), &(-10..10)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-100..0), &(-10..10)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-100..10), &(-10..10)), Contains);
    assert_eq!(RangeExt::rel(&(-10..10), &(-10..10)), Same);
    assert_eq!(RangeExt::rel(&(-10..100), &(-10..10)), Contains);
    assert_eq!(RangeExt::rel(&(0..100), &(-10..10)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(10..100), &(-10..10)), AfterEnd);
    assert_eq!(RangeExt::rel(&(20..100), &(-10..10)), AfterEnd);

    assert_eq!(RangeExt::rel(&(-10..10), &(20..100)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-10..10), &(10..100)), BeforeStart);
    assert_eq!(RangeExt::rel(&(-10..10), &(0..100)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-10..10), &(-10..100)), Contained);
    assert_eq!(RangeExt::rel(&(-10..10), &(-10..10)), Same);
    assert_eq!(RangeExt::rel(&(-10..10), &(-100..10)), Contained);
    assert_eq!(RangeExt::rel(&(-10..10), &(-100..0)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(-10..10), &(-100..-10)), AfterEnd);
    assert_eq!(RangeExt::rel(&(-10..10), &(-100..-20)), AfterEnd);
}

#[test]
fn test_exclusive_range_edges_el() {
    use RangeRelation::*;

    assert_eq!(RangeExt::rel(&(-100..-9), &(-10..10)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(9..100), &(-10..10)), IntersectsEnd);
    assert_eq!(RangeExt::rel(&(-10..10), &(9..100)), IntersectsStart);
    assert_eq!(RangeExt::rel(&(-10..10), &(-100..-9)), IntersectsEnd);
}
