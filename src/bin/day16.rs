use aoc::*;

// 2289 is too low

#[derive(Inpt, Debug)]
#[inpt(regex = r"Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves?")]
struct Valve {
    name: String,
    flow: u64,
    #[inpt(after)]
    adj: Vec<Word<String>>,
}

type Input = Vec<Line<Valve>>;
type Graph = Vec<(Vec<usize>, u64, String)>;

fn graph(input: &Input) -> (Graph, M<&str, usize>) {
    let mut inds = M::default();
    for (i, valve) in input.iter().enumerate() {
        inds.insert(valve.inner.name.as_str(), i);
    }
    let graph = input
        .iter()
        .map(|valve| {
            (
                valve
                    .inner
                    .adj
                    .iter()
                    .map(|name| inds[name.inner.as_str()])
                    .collect(),
                valve.inner.flow,
                valve.inner.name.clone(),
            )
        })
        .collect();
    (graph, inds)
}

fn solve_part1(
    graph: &Graph,
    table: &mut M<(u64, usize, u64), (u64, String)>,
    t: u64,
    end: u64,
    pos: usize,
    open: u64,
) -> u64 {
    if t >= end {
        return 0;
    }
    if open == (1 << graph.len()) - 1 {
        return 0;
    }
    let mut new = (t, pos, open);
    if let Some(&(p, _)) = table.get(&new) {
        return p;
    }
    let mut p = 0;
    let mut act = "NONE";
    if t < end + 1 && !new.2.get_bit(pos) && graph[pos].1 > 0 {
        new.2 = new.2.set_bit(pos, true);
        let gain = graph[pos].1 * (end - t - 1);
        let this_p = solve_part1(graph, table, t + 1, end, pos, new.2.clone()) + gain;
        if this_p > p {
            p = this_p;
            act = "OPEN";
        }
        new.2 = new.2.set_bit(pos, false);
    }
    if t < end + 2 {
        for &adj in &graph[pos].0 {
            let this_p = solve_part1(graph, table, t + 1, end, adj, new.2.clone());
            if this_p > p {
                p = this_p;
                act = &graph[adj].2;
            }
        }
    }
    table.insert(new, (p, act.to_string()));
    p
}

fn part1(input: Input) -> u64 {
    let (g, inds) = graph(&input);
    let mut table = M::default();
    let max_p = solve_part1(&g, &mut table, 0, 30, inds["AA"], 0);
    #[cfg(feature = "visulize")]
    {
        let mut state = (0, inds["AA"], 0);
        while state.0 < 30 {
            if state.2 == (1 << input.len()) - 1 {
                break;
            }
            let Some((p, act)) = table.get(&state) else { panic!("unchecked state: {state:?}"); };
            if act == "NONE" {
                break;
            }
            if act == "OPEN" {
                state.2.set_bit(state.1, true);
            } else {
                state.1 = inds[act.as_str()];
            }
            state.0 += 1;
            println!("{}: {} open, {} pressure", state.1, state.2.count_ones(), p);
        }
    }
    max_p
}

fn solve_part2(
    graph: &Graph,
    table: &mut M<(u64, usize, usize, u64), u64>,
    t: u64,
    end: u64,
    pos1: usize,
    pos2: usize,
    open: u64,
) -> u64 {
    if t >= end {
        return 0;
    }
    if open == (1 << graph.len()) - 1 {
        return 0;
    }
    let mut new = (t, pos2, pos1, open);
    if let Some(&p) = table.get(&new) {
        return p;
    }
    swap(&mut new.1, &mut new.2);
    if let Some(&p) = table.get(&new) {
        return p;
    }
    let mut p = 0;
    let can_open1 = t < end + 1 && !new.3.get_bit(pos1) && graph[pos1].1 > 0;
    let can_open2 = t < end + 1 && !new.3.get_bit(pos2) && graph[pos2].1 > 0 && pos1 != pos2;
    let can_move1 = t < end + 2;
    let can_move2 = t < end + 2;
    if can_open1 && can_open2 {
        new.3 = new.3.set_bit(pos1, true);
        new.3 = new.3.set_bit(pos2, true);
        let gain = (graph[pos1].1 + graph[pos2].1) * (end - t - 1);
        let this_p = solve_part2(graph, table, t + 1, end, pos1, pos2, new.3.clone()) + gain;
        if this_p > p {
            p = this_p;
        }
        new.3 = new.3.set_bit(pos1, false);
        new.3 = new.3.set_bit(pos2, false);
    }
    if can_open1 && can_move2 {
        new.3 = new.3.set_bit(pos1, true);
        let gain = graph[pos1].1 * (end - t - 1);
        for &adj2 in &graph[pos2].0 {
            let this_p = solve_part2(graph, table, t + 1, end, pos1, adj2, new.3.clone()) + gain;
            if this_p > p {
                p = this_p;
            }
        }
        new.3 = new.3.set_bit(pos1, false);
    }
    if can_move1 && can_open2 {
        new.3 = new.3.set_bit(pos2, true);
        let gain = graph[pos2].1 * (end - t - 1);
        for &adj1 in &graph[pos1].0 {
            let this_p = solve_part2(graph, table, t + 1, end, adj1, pos2, new.3.clone()) + gain;
            if this_p > p {
                p = this_p;
            }
        }
        new.3 = new.3.set_bit(pos2, false);
    }
    if can_move1 && can_move2 {
        for &adj1 in &graph[pos1].0 {
            for &adj2 in &graph[pos2].0 {
                let this_p = solve_part2(graph, table, t + 1, end, adj1, adj2, new.3.clone());
                if this_p > p {
                    p = this_p;
                }
            }
        }
    }
    table.insert(new, p);
    p
}

fn part2(input: Input) -> u64 {
    let (g, inds) = graph(&input);
    let mut table = M::default();
    let max_p = solve_part2(&g, &mut table, 0, 26, inds["AA"], inds["AA"], 0);
    max_p
}

fn main() {
    run(part1);
    //run(part2);
}
