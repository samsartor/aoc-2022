use aoc::*;

fn solve(input: Vec<(char, isize)>, train_len: usize) -> usize {
    let mut train = vec![(0isize, 0isize); train_len];
    #[cfg(feature = "visulize")]
    let mut grid = Grid::fill(1, 1, |_, _| '.').pad(15, 15, 15, 15, '.');
    let mut touched = S::default();
    for (dir, dist) in input {
        for _ in 0..dist {
            #[cfg(feature = "visulize")]
            for &(x, y) in &train {
                grid.set(x, y, '.');
            }
            match dir {
                'U' => train[0].1 -= 1,
                'D' => train[0].1 += 1,
                'L' => train[0].0 -= 1,
                'R' => train[0].0 += 1,
                _ => panic!(),
            };
            for i in 1..train.len() {
                let dx = train[i - 1].0 - train[i].0;
                let dy = train[i - 1].1 - train[i].1;
                if dx.abs() > 1 || dy.abs() > 1 {
                    train[i].0 += dx.signum();
                    train[i].1 += dy.signum();
                }
            }
            #[cfg(feature = "visulize")]
            for i in (0..train.len()).rev() {
                grid.set(
                    train[i].0,
                    train[i].1,
                    ['H', '1', '2', '3', '4', '5', '6', '7', '8', '9'][i],
                );
            }
            touched.insert(*train.last().unwrap());
        }
        #[cfg(feature = "visulize")]
        println!("=={dir} {dist}==\n{grid}")
    }
    touched.len()
}

fn part1(input: Vec<(char, isize)>) -> usize {
    solve(input, 2)
}

fn part2(input: Vec<(char, isize)>) -> usize {
    solve(input, 10)
}

fn main() {
    run(part1);
    run(part2);
}
