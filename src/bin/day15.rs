use aoc::*;

#[derive(Inpt, Debug, Copy, Clone)]
struct Pt {
    #[inpt(trim = r"^\-\d")]
    x: isize,
    #[inpt(trim = r"^\-\d")]
    y: isize,
}

impl Pt {
    fn dist(&self, other: &Self) -> isize {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

#[derive(Inpt, Debug)]
struct In {
    s: Pt,
    b: Pt,
}

type Input = Vec<In>;

fn part1(input: Input) -> usize {
    let row = if real() { 2_000_000 } else { 10 };

    let mut grid: Grid;
    #[cfg(feature = "visulize")]
    {
        grid = Grid::fill(100, 100, |_, _| ' ');
        grid.offset_y = row - 50;
        grid.offset_x = -50;
    }

    let mut set = S::default();
    for In { s, b } in &input {
        let dist = s.dist(&b);

        #[cfg(feature = "visulize")]
        {
            grid.set(s.x, s.y, 'S');
            grid.set(b.x, b.y, 'B');
            for (x, y) in grid.coords() {
                if s.dist(&Pt { x, y }) <= dist {
                    if grid.at(x, y) == ' ' {
                        grid.set(x, y, '#');
                    }
                }
            }
        }

        let rad = dist - (s.y - row).abs();
        if rad >= 0 {
            for x_off in -rad..=rad {
                set.insert(s.x + x_off);
            }
        }
    }
    for &In { b, .. } in &input {
        if b.y == row {
            set.remove(&b.x);
        }
    }

    #[cfg(feature = "visulize")]
    {
        for &x in &set {
            grid.set(x, row, '!');
        }
        println!("{}", grid);
    }

    set.len()
}

fn part2(input: Input) -> isize {
    let size = if real() { 4_000_000 } else { 20 };

    let mut grid: Grid;
    #[cfg(feature = "visulize")]
    {
        grid = Grid::fill(100, 100, |_, _| ' ');
        grid.offset_x = -50;
        grid.offset_y = -50;
    }

    let mut output = 0;
    for &In { s, b } in &input {
        let width = s.dist(&b) + 1;
        let mut check_edge = |ox, oy, dx, dy| {
            for i in 0..width {
                let x = s.x + ox + i * dx;
                let y = s.y + oy + i * dy;

                #[cfg(feature = "visulize")]
                grid.set(x, y, '#');

                if (0..=size).contains(&x)
                    && (0..=size).contains(&y)
                    && input
                        .iter()
                        .all(|&In { s, b }| s.dist(&Pt { x, y }) > s.dist(&b))
                {
                    #[cfg(feature = "visulize")]
                    grid.set(x, y, '!');
                    output = x * 4_000_000 + y;
                    return;
                }
            }
        };

        check_edge(0, width, 1, -1);
        check_edge(width, 0, -1, -1);
        check_edge(0, -width, -1, 1);
        check_edge(-width, 0, 1, 1)
    }
    #[cfg(feature = "visulize")]
    println!("{}", grid);
    output
}

fn main() {
    run(part1);
    run(part2);
}
