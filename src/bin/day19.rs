#![feature(array_zip)]

use std::cmp::Reverse;

use aoc::*;

type N = u32;

#[derive(Inpt, Debug)]
#[inpt(regex = r"Each (\w+) robot costs (\d+ \w+)(?: and (\d+ \w+))?.")]
struct Robot {
    ty: String,
    cost1: (N, String),
    cost2: Option<(N, String)>,
}

#[derive(Inpt, Debug)]
#[inpt(regex = r"Blueprint (\d+):")]
struct Blueprint {
    n: N,
    #[inpt(after, split = "Line")]
    bots: Vec<Robot>,
}

#[derive(Inpt, Debug)]
#[inpt(from = "Vec<Blueprint>")]
pub struct Input {
    blueprints: Vec<(N, [[N; 4]; 4])>,
}

fn ty2ind(ty: &str) -> usize {
    match ty {
        "ore" => 0,
        "clay" => 1,
        "obsidian" => 2,
        "geode" => 3,
        _ => panic!("unknown type {ty:?}"),
    }
}

impl From<Vec<Blueprint>> for Input {
    fn from(value: Vec<Blueprint>) -> Self {
        let blueprints = value
            .into_iter()
            .map(|b| {
                let mut out = [[0; 4]; 4];
                for Robot { ty, cost1, cost2 } in &b.bots {
                    out[ty2ind(ty)][ty2ind(&cost1.1)] = cost1.0;
                    if let Some(cost2) = cost2 {
                        out[ty2ind(ty)][ty2ind(&cost2.1)] = cost2.0;
                    }
                }
                (b.n, out)
            })
            .collect();
        Input { blueprints }
    }
}

fn use_cost(val: [N; 4], cost: [N; 4]) -> Option<[N; 4]> {
    Some([
        val[0].checked_sub(cost[0])?,
        val[1].checked_sub(cost[1])?,
        val[2].checked_sub(cost[2])?,
        val[3].checked_sub(cost[3])?,
    ])
}

fn add_mined(val: [N; 4], bots: [N; 4]) -> [N; 4] {
    val.zip(bots).map(|(a, b)| a + b)
}

fn eval_blueprint(
    bprint: &[[N; 4]; 4],
    rem: N,
    with: [N; 4],
    bots: [N; 4],
    table: &mut M<(N, [N; 4], [N; 4]), ([N; 4], Option<usize>)>,
) -> [N; 4] {
    if rem == 0 {
        return with;
    }
    if let Some((val, _)) = table.get(&(rem, with, bots)) {
        return *val;
    }
    let mut action = None;
    let mut best = eval_blueprint(bprint, rem - 1, add_mined(with, bots), bots, table);
    for (i, &cost) in bprint.iter().enumerate() {
        if let Some(with) = use_cost(with, cost) {
            let with = add_mined(with, bots);
            let mut bots = bots;
            bots[i] += 1;
            let this = eval_blueprint(bprint, rem - 1, with, bots, table);
            if this[3] > best[3] {
                best = this;
                action = Some(i);
            }
        }
    }
    //table.insert((rem, with, bots), (best, action));
    best
}

fn part1(Input { blueprints }: Input) -> u32 {
    let values: Vec<_> = blueprints
        .into_iter()
        .map(|(n, b)| {
            let mut table = M::default();
            let mut with = [0, 0, 0, 0];
            let mut bots = [1, 0, 0, 0];
            let mut rem = 24;
            let val = eval_blueprint(&b, rem, with, bots, &mut table);
            #[cfg(feature = "visulize")]
            {
                eprintln!("=== Blueprint {n} ===");
                eprintln!("{b:?}");
                while rem > 0 {
                    let (_, action) = table[&(rem, with, bots)];
                    match action {
                        Some(x) => {
                            eprintln!("{with:?}, {bots:?} => build bot {x}");
                            with = use_cost(with, b[x]).unwrap();
                            with = add_mined(with, bots);
                            bots[x] += 1;
                        }
                        None => {
                            eprintln!("{with:?}, {bots:?} => none");
                            with = add_mined(with, bots);
                        }
                    }
                    rem -= 1;
                }
            }
            (n, val)
        })
        .collect();
    dbg!(&values);
    values.iter().map(|(n, v)| n * v[3]).sum()
}

fn part2(Input { blueprints }: Input) -> u64 {
    0
}

fn main() {
    run(part1);
    run(part2);
}
