#![feature(array_windows)]
use aoc::*;

fn solve_n2<const N: usize>(x: &[char]) -> usize {
    x.array_windows::<N>()
        .position(|w| {
            for i in 0..N - 1 {
                for j in (i + 1)..N {
                    if w[i] == w[j] {
                        return false;
                    }
                }
            }
            true
        })
        .unwrap()
        + N
}

fn part1(input: Vec<char>) -> usize {
    solve_n2::<4>(&input)
}

fn part2(input: Vec<char>) -> usize {
    solve_n2::<14>(&input)
}

fn main() {
    run(part1);
    run(part2);
}
