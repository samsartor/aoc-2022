use aoc::*;

#[derive(Inpt, Debug, Copy, Clone)]
enum Jet {
    #[inpt(regex = "<")]
    Left,
    #[inpt(regex = ">")]
    Right,
}

type Input = Vec<Jet>;

const BLOCKS: &[&str] = &[
    "####",
    ".#.\n###\n.#.",
    "###\n..#\n..#",
    "#\n#\n#\n#",
    "##\n##",
];

fn solve(input: Input, steps: usize) -> isize {
    let mut grid = Grid::fill(7, 0, |_, _| false);
    let blocks: Vec<_> = BLOCKS
        .into_iter()
        .map(|text| inpt::<Grid>(text).unwrap().map(|_, _, c| c == '#'))
        .collect();
    let mut top = 0;
    let mut block = 0;
    let mut x = 2;
    let mut y = 3;
    let mut jet = 0;

    grid = grid.view(0, grid.width(), 0, blocks[block].height() + y, false);

    #[allow(unused)]
    let show_grid = |grid: &Grid<bool>, block: &Grid<bool>, x: isize, y: isize, d: String| {
        let mut dbg_grid = Grid::fill(grid.w, grid.h, |_, _| '.');
        for (cx, cy) in block.coords() {
            if block.at(cx, cy) {
                dbg_grid.set(x + cx, grid.h as isize - (y + cy) - 1, '@');
            }
        }
        for (x, y) in grid.coords() {
            if grid.at(x, y) {
                dbg_grid.set(x, grid.h as isize - y - 1, '#')
            }
        }
        println!("\n======= {d:?}\n{dbg_grid}")
    };

    #[cfg(feature = "visulize")]
    show_grid(&grid, &blocks[block], x, y, format!("New block"));
    while block < steps {
        // Move block left/right
        let b = &blocks[block % blocks.len()];
        let j = input[jet % input.len()];
        let new_x = x + match j {
            Jet::Left => -1,
            Jet::Right => 1,
        };
        if new_x >= 0
            && new_x as usize + b.w <= grid.w
            && !b
                .coords()
                .any(|(cx, cy)| b.at(cx, cy) && grid.at(new_x + cx, y + cy))
        {
            x = new_x;
        }
        #[cfg(feature = "visulize")]
        show_grid(&grid, b, x, y, format!("{j:?}"));

        // Update jet
        jet += 1;

        // Try to move block down
        if y == 0
            || b.coords()
                .any(|(cx, cy)| b.at(cx, cy) && grid.at(x + cx, y - 1 + cy))
        {
            // Place block
            for (cx, cy) in b.coords() {
                if b.at(cx, cy) {
                    grid.set(x + cx, y + cy, true);
                    top = (y + cy).max(top);
                }
            }

            #[cfg(feature = "visulize")]
            show_grid(&grid, b, x, y, format!("Placed"));

            // Update state
            block += 1;
            let b = &blocks[block % blocks.len()];
            x = 2;
            y = top + 4;
            grid = grid.view(0, grid.width(), grid.offset_y, b.height() + y, false);
            #[cfg(feature = "visulize")]
            show_grid(&grid, b, x, y, format!("New block"));
        } else {
            // Move block down
            y -= 1;
            #[cfg(feature = "visulize")]
            show_grid(
                &grid,
                &blocks[block % blocks.len()],
                x,
                y,
                format!("Move down"),
            );
        }
    }

    top + 1
}

fn part1(input: Input) -> isize {
    #[cfg(feature = "visulize")]
    return solve(input, 10);
    #[cfg(not(feature = "visulize"))]
    return solve(input, 2022);
}

fn part2(input: Input) -> isize {
    panic!();
    solve(input, 1000000000000)
}

fn main() {
    run(part1);
    run(part2);
}
