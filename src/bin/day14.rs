#![feature(array_windows)]
use aoc::*;

#[derive(Inpt, Debug)]
#[inpt(trim = r"\s\->", regex = r"(\d+),(\d+)")]
struct In(isize, isize);
type Input = Vec<Line<Vec<In>>>;

fn add_sand(g: &mut Grid<char>) -> bool {
    let mut x = 500;
    let mut y = 0;
    let Some(&cur) = g.get(x, y) else { return false };
    if cur != '.' {
        return false;
    };
    loop {
        let Some(&below) = g.get(x, y+1) else { return false };
        if below == '.' {
            y += 1;
            continue;
        };
        let Some(&left) = g.get(x-1, y+1) else { return false };
        if left == '.' {
            y += 1;
            x -= 1;
            continue;
        }
        let Some(&right) = g.get(x+1, y+1) else { return false };
        if right == '.' {
            y += 1;
            x += 1;
            continue;
        }
        break;
    }
    g.set(x, y, 'o');
    true
}

fn part1(input: Input) -> u64 {
    let x0 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.0)
        .min()
        .unwrap() as usize;
    let x1 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.0)
        .max()
        .unwrap() as usize;
    let y0 = 0; /*input
                .iter()
                .flat_map(|i| &i.inner)
                .map(|i| i.1)
                .min()
                .unwrap() as usize;*/
    let y1 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.1)
        .max()
        .unwrap() as usize;
    let mut grid = Grid::fill(x1 - x0 + 1, y1 - y0 + 1, |_, _| '.');
    grid.offset_x = x0 as isize;
    grid.offset_y = y0 as isize;

    for path in &input {
        for [In(mut x, mut y), In(ex, ey)] in path.inner.array_windows() {
            grid.set(x, y, '#');
            loop {
                x += (ex - x).signum();
                y += (ey - y).signum();
                grid.set(x, y, '#');
                if x == *ex && y == *ey {
                    break;
                }
            }
        }
    }
    let mut count = 0;
    while add_sand(&mut grid) {
        count += 1;
    }
    println!("{}", &grid);
    count
}

fn part2(mut input: Input) -> u64 {
    let mut x0 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.0)
        .min()
        .unwrap();
    let mut x1 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.0)
        .max()
        .unwrap();
    let y0 = 0; /*input
                .iter()
                .flat_map(|i| &i.inner)
                .map(|i| i.1)
                .min()
                .unwrap() as usize;*/
    let y1 = input
        .iter()
        .flat_map(|i| &i.inner)
        .map(|i| i.1)
        .max()
        .unwrap()
        + 2;
    x0 -= y1;
    x1 += y1;
    input.push(Line {
        inner: vec![In(x0, y1), In(x1, y1)],
    });
    let mut grid = Grid::fill((x1 - x0 + 1) as usize, (y1 - y0 + 1) as usize, |_, _| '.');
    grid.offset_x = x0 as isize;
    grid.offset_y = y0 as isize;

    for path in &input {
        for [In(mut x, mut y), In(ex, ey)] in path.inner.array_windows() {
            grid.set(x, y, '#');
            loop {
                x += (ex - x).signum();
                y += (ey - y).signum();
                grid.set(x, y, '#');
                if x == *ex && y == *ey {
                    break;
                }
            }
        }
    }
    let mut count = 0;
    while add_sand(&mut grid) {
        count += 1;
    }
    println!("{}", &grid);
    count
}

fn main() {
    run(part1);
    run(part2);
}
