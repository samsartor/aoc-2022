use aoc::*;
use std::{
    cmp::Reverse,
    collections::VecDeque,
    ops::{Div, Rem},
};

#[derive(Inpt, Debug)]
enum Op {
    #[inpt(regex = r"old \* old")]
    MulOld,
    #[inpt(regex = r"old \+ (\d+)")]
    Add(u64),
    #[inpt(regex = r"old \* (\d+)")]
    Mul(u64),
}

#[derive(Inpt, Debug)]
#[inpt(
    regex = r"Monkey (?:\d+):\s+Starting items:([^\n]+)\s+Operation: new = ([^\n]+)\s+Test: divisible by (\d+)\s+If true: throw to monkey (\d+)\s+If false: throw to monkey (\d+)"
)]
struct Monkey {
    #[inpt(from_iter = "u64")]
    items: VecDeque<u64>,
    op: Op,
    test: u64,
    if_true: usize,
    if_false: usize,
}

fn solve(mut m: Vec<Monkey>, time: usize, divisor: u64) -> usize {
    let mut times = vec![0; m.len()];
    let modulo: u64 = m.iter().map(|m| m.test).product();
    for _ in 0..time {
        for i in 0..m.len() {
            while let Some(worry) = m[i].items.pop_front() {
                times[i] += 1;
                let worry = match m[i].op {
                    Op::MulOld => worry * worry,
                    Op::Add(v) => worry + v,
                    Op::Mul(v) => worry * v,
                }
                .div(divisor)
                .rem(modulo);
                let j = if worry % m[i].test == 0 {
                    m[i].if_true
                } else {
                    m[i].if_false
                };
                m[j].items.push_back(worry);
            }
        }
    }
    times.sort_by_key(|t| Reverse(*t));
    times[0] * times[1]
}

fn part1(m: Vec<Monkey>) -> usize {
    solve(m, 20, 3)
}

fn part2(m: Vec<Monkey>) -> usize {
    solve(m, 10_000, 1)
}

fn main() {
    run(part1);
    run(part2);
}
