use std::collections::VecDeque;

use aoc::*;
/*
#[derive(Inpt, Debug)]
//#[inpt(regex = r"")]
struct Input {

}
*/

type Input = Grid<char>;

fn solve(input: &Input, start_x: isize, start_y: isize) -> u64 {
    let mut queue = VecDeque::new();
    let mut set = S::default();
    queue.push_back((0, start_x, start_y));
    while let Some((len, x, y)) = queue.pop_front() {
        if !set.insert((x, y)) {
            continue;
        }
        let cur = input.at(x, y);
        if cur == 'E' {
            return len;
        }
        for (i, j) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            let Some(&nex) = input.get(x+i, y+j) else { continue };
            let valid = match (cur, nex) {
                ('S', _) => true,
                ('y'..='z', 'E') => true,
                ('a'..='z', 'a'..='z') => (nex as i32) - (cur as i32) <= 1,
                _ => false,
            };
            if valid {
                queue.push_back((len + 1, x + i, y + j));
            }
        }
    }
    u64::MAX
}

fn part1(input: Input) -> u64 {
    let (sx, sy) = input
        .coords()
        .find(|&(x, y)| input.at(x, y) == 'S')
        .unwrap();
    solve(&input, sx, sy)
}

fn part2(input: Input) -> u64 {
    input
        .coords()
        .filter(|&(x, y)| input.at(x, y) == 'a')
        .map(|(sx, sy)| solve(&input, sx, sy))
        .min()
        .unwrap()
}

fn main() {
    run(part1);
    run(part2);
}
