use aoc::*;
use std::cmp::Reverse;

fn part1(input: Vec<Group<Vec<usize>>>) -> usize {
    input
        .iter()
        .map(|elf| elf.inner.iter().sum())
        .max()
        .unwrap()
}

fn part2(input: Vec<Group<Vec<usize>>>) -> usize {
    let mut totals: Vec<_> = input
        .iter()
        .map(|elf| elf.inner.iter().sum::<usize>())
        .collect();
    totals.sort_by_key(|x| Reverse(*x));
    totals[0..3].iter().sum()
}

fn main() {
    run(part1);
    run(part2);
}
