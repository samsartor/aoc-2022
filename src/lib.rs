#![allow(incomplete_features)]
#![feature(
    never_type,
    specialization,
    return_position_impl_trait_in_trait,
    type_alias_impl_trait
)]

use once_cell::sync::OnceCell;
use regex::Regex;
use std::any::type_name;
use std::env::{args, var};
use std::fmt;
use std::fs;
use std::io;
use std::io::Read;
use std::path::MAIN_SEPARATOR;
use std::process::exit;
use std::time::Instant;
use termion::color;
use termion::style;

mod bitreader;
mod grid;
mod tools;

pub use crate::bitreader::*;
pub use crate::grid::*;
pub use crate::tools::*;

pub use inpt::split::*;
pub use inpt::{inpt, Inpt};

pub static INPUT: OnceCell<String> = OnceCell::new();

pub fn real() -> bool {
    static REAL: OnceCell<bool> = OnceCell::new();
    *REAL.get_or_init(|| var("AOC_OUTPUT").is_ok())
}

pub fn run<'s, I, F, D>(func: F)
where
    F: Fn(I) -> D,
    I: Inpt<'s>,
    D: fmt::Display,
{
    let func_name = type_name::<F>();
    let parsed_name = Regex::new(r"^day(\d+)::part(\d+)$")
        .unwrap()
        .captures(func_name);

    let arg = args().skip(1).next();
    if arg.as_ref().map(|a| a != func_name).unwrap_or(false) {
        return;
    }

    let testing = var("AOC_TESTING").is_ok();

    let name;
    let path;
    if let Some(captures) = parsed_name {
        let day: u32 = captures[1].parse().unwrap();
        let part: u32 = captures[2].parse().unwrap();
        name = format!("day {} part {}", day, part);
        path = Some(format!(
            "outputs{}day{:02}_{}.txt",
            MAIN_SEPARATOR, day, part
        ))
    } else {
        name = func_name.to_string();
        path = None;
    };

    let buf = match INPUT.get_or_try_init(|| -> Result<String, io::Error> {
        let mut buf = String::new();
        io::stdin().lock().read_to_string(&mut buf)?;
        Ok(buf)
    }) {
        Err(err) => {
            println!(
                "{}{}INPUT ERROR{} in {}:{}{} {}",
                style::Italic,
                color::Fg(color::Red),
                color::Fg(color::White),
                name,
                style::Reset,
                color::Fg(color::Reset),
                err
            );
            return;
        }
        Ok(b) => b,
    };

    let input_str = buf.as_str();
    let input = match inpt(input_str) {
        Ok(i) => i,
        Err(e) => {
            let _ = e.annotated_stdout(&name);
            return;
        }
    };

    let start = Instant::now();
    let mut output = func(input).to_string();
    let mut elapsed = start.elapsed();

    if let Some(n) = var("AOC_BENCH").ok().and_then(|n| n.parse::<u32>().ok()) {
        for _ in 1..n {
            let input = inpt(input_str).expect("input parsing is unreliable during benchmarking");
            let start = Instant::now();
            func(input).to_string();
            elapsed += start.elapsed();
        }
        elapsed /= n;
    }

    while output.ends_with('\n') {
        output.pop();
    }
    println!(
        "{}{}OUTPUT{} {}:{}{} {}{}{}{}",
        style::Italic,
        color::Fg(color::Blue),
        color::Fg(color::White),
        name,
        style::Reset,
        color::Fg(color::Reset),
        style::Bold,
        if output.contains('\n') { "\n" } else { "" },
        output,
        style::Reset,
    );
    println!("\tDone in {}", humantime::format_duration(elapsed));
    if let Some(path) = path {
        if testing {
            match &fs::read_to_string(path) {
                Ok(existing) if existing.trim() == output.trim() => {
                    println!(
                        "\t{}TEST PASSED{}",
                        color::Fg(color::Green),
                        color::Fg(color::Reset)
                    );
                }
                Ok(existing) => {
                    println!(
                        "\t{}TEST EXPECTED {:?}{}",
                        color::Fg(color::Red),
                        existing.trim(),
                        color::Fg(color::Reset)
                    );
                    exit(1);
                }
                Err(e) => {
                    println!("\tCould not read input: {}", e);
                    exit(1);
                }
            }
        } else if real() {
            output += "\n";
            let _ = fs::write(path, output);
        }
    }
}
