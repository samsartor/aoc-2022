use aoc::*;

#[derive(Inpt, Debug)]
enum Input<'s> {
    #[inpt(regex = r"\$ cd ([^\n]+)")]
    Cd(&'s str),
    #[inpt(regex = r"\$ ls")]
    Ls,
    #[inpt(regex = r"dir ([^\n]+)")]
    Dir(&'s str),
    #[inpt(regex = r"(\d+) ([^\n]+)")]
    File(usize, &'s str),
}

#[derive(Inpt, Debug, Default)]
#[inpt(from = "Vec<Input<'s>>")]
struct Tree<'s> {
    sizes: M<Vec<&'s str>, usize>,
    dirs: M<Vec<&'s str>, Vec<&'s str>>,
}

impl<'s> From<Vec<Input<'s>>> for Tree<'s> {
    fn from(cmds: Vec<Input<'s>>) -> Self {
        let mut this = Tree::default();
        let mut path = Vec::new();
        for cmd in cmds {
            match cmd {
                Input::Cd(name) => {
                    if name == ".." {
                        path.pop();
                    } else {
                        path.push(name);
                    }
                }
                Input::Ls => (),
                Input::Dir(name) => {
                    this.dirs.entry(path.clone()).or_default().push(name);
                }
                Input::File(size, name) => {
                    this.dirs.entry(path.clone()).or_default().push(name);
                    let mut path = path.clone();
                    path.push(name);
                    this.sizes.insert(path, size);
                }
            }
        }
        this
    }
}

impl<'s> Tree<'s> {
    fn sum_directories(
        &self,
        cd: &mut Vec<&'s str>,
        func: &mut impl FnMut(&Vec<&'s str>, usize),
    ) -> usize {
        let mut sum = 0;
        if let Some(size) = self.sizes.get(cd) {
            sum += size;
        }
        if let Some(dir) = self.dirs.get(cd) {
            for child in dir {
                cd.push(child.clone());
                sum += self.sum_directories(cd, func);
                cd.pop();
            }
            func(&cd, sum);
        }
        sum
    }
}

fn part1(tree: Tree) -> usize {
    let mut sum = 0;
    tree.sum_directories(&mut vec!["/"], &mut |_path, size| {
        if size <= 100000 {
            sum += size;
        }
    });
    sum
}

fn part2(tree: Tree) -> usize {
    let current_total = tree.sum_directories(&mut vec!["/"], &mut |_, _| {});
    let current_free = 70_000_000 - current_total;
    let size_goal = 30_000_000 - current_free;
    let mut best = usize::MAX;
    tree.sum_directories(&mut vec!["/"], &mut |_path, size| {
        if size >= size_goal && size < best {
            best = size;
        }
    });
    best
}

fn main() {
    run(part1);
    run(part2);
}
