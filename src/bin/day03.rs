use aoc::*;

fn score(c: char) -> i32 {
    if ('a'..='z').contains(&c) {
        c as i32 - 'a' as i32 + 1
    } else {
        c as i32 - 'A' as i32 + 27
    }
}

fn part1_impl(sack: Line<Vec<char>>) -> i32 {
    let (a, b) = sack.inner.split_at(sack.inner.len() / 2);
    let a = S::from_iter(a);
    let b = S::from_iter(b);
    let same = a.intersection(&b).next().unwrap();
    score(**same)
}

fn part1(input: Vec<Line<Vec<char>>>) -> i32 {
    input.into_iter().map(|s| part1_impl(s)).sum()
}

fn part2_impl(sacks: [Line<Vec<char>>; 3]) -> i32 {
    let [a, b, c] = sacks.map(|sack| S::from_iter(sack.inner.into_iter()));
    let ab = S::from_iter(a.intersection(&b).copied());
    let same = ab.intersection(&c).next().unwrap();
    score(*same)
}

fn part2(input: Vec<[Line<Vec<char>>; 3]>) -> i32 {
    input.into_iter().map(|g| part2_impl(g)).sum()
}

fn main() {
    run(part1);
    run(part2);
}
