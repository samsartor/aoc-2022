use aoc::*;

#[derive(Inpt)]
#[inpt(trim = r"\s")]
enum Seg {
    Num(isize),
    #[inpt(regex = "L")]
    Left,
    #[inpt(regex = "R")]
    Right,
}

#[derive(Inpt)]
#[inpt(trim = "")]
struct Input(Grid, Vec<Seg>);

fn part1(Input(map, path): Input) -> isize {
    let mut y = 0;
    let mut x = (0..map.width()).position(|x| map.at(x, y) == '.').unwrap() as isize;
    let mut dir = 0isize;
    let mut vis = map.clone();
    for seg in path {
        let (dx, dy) = match dir {
            0 => (1, 0),
            1 => (0, 1),
            2 => (-1, 0),
            3 => (0, -1),
            _ => panic!(),
        };
        match seg {
            Seg::Num(mut dist) => {
                while dist > 0 {
                    vis.set(
                        x,
                        y,
                        match dir {
                            0 => '>',
                            1 => 'v',
                            2 => '<',
                            3 => '^',
                            _ => panic!(),
                        },
                    );
                    let (mut nx, mut ny) = (x + dx, y + dy);
                    match map.get(nx, ny) {
                        None | Some(' ') => {
                            nx = match dx {
                                -1 => map.width() - 1,
                                0 => x,
                                1 => 0,
                                _ => panic!(),
                            };
                            ny = match dy {
                                -1 => map.height() - 1,
                                0 => y,
                                1 => 0,
                                _ => panic!(),
                            };
                            loop {
                                match map.at(nx, ny) {
                                    '.' => {
                                        x = nx;
                                        y = ny;
                                        break;
                                    }
                                    '#' => break,
                                    _ => (),
                                }
                                nx += dx;
                                ny += dy;
                            }
                            dist -= 1;
                        }
                        Some('.') => {
                            x = nx;
                            y = ny;
                            dist -= 1;
                        }
                        Some('#') => {
                            dist = 0;
                        }
                        _ => panic!(),
                    };
                }
            }
            Seg::Left => dir = (dir - 1).rem_euclid(4),
            Seg::Right => dir = (dir + 1).rem_euclid(4),
        }
    }
    vis.set(x, y, 'X');
    eprintln!("{}", vis);

    1000 * (y + 1) + 4 * (x + 1) + dir
}

fn part2(Input(map, path): Input) -> u64 {
    0
}

fn main() {
    run(part1);
    run(part2);
}
