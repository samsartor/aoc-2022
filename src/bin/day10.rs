#![allow(non_snake_case)]

use aoc::*;

#[derive(Inpt, Debug)]
enum Inst {
    #[inpt(regex = r"noop")]
    Noop,
    #[inpt(regex = r"addx")]
    Addx(#[inpt(after)] isize),
}

fn interpret(program: Vec<Inst>, mut each: impl FnMut(isize, isize)) {
    let mut X = 1;
    let mut clock = 1;
    let mut each = |X| {
        each(clock, X);
        clock += 1;
    };
    for inst in program {
        match inst {
            Inst::Noop => {
                each(X);
            }
            Inst::Addx(v) => {
                each(X);
                each(X);
                X += v;
            }
        }
    }
}

fn part1(program: Vec<Inst>) -> isize {
    let mut sum = 0;
    interpret(program, |clock, X| {
        if [20, 60, 100, 140, 180, 220].contains(&clock) {
            let strength = clock * X;
            sum += strength;
        }
    });
    sum
}

fn part2(program: Vec<Inst>) -> Grid<char> {
    let mut grid = Grid::fill(40, 6, |_, _| ' ');
    interpret(program, |clock, X| {
        let px = (clock - 1) % 40;
        let py = (clock - 1) / 40;
        if X.abs_diff(px) <= 1 {
            grid.set(px as isize, py as isize, '#');
        }
    });
    grid
}

fn main() {
    run(part1);
    run(part2);
}
