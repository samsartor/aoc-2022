use inpt::split::Group;
use inpt::{CharClass, Inpt, InptIter, InptStep, RecursionGuard, WHITESPACE};
use inpt::{InptError, InptResult, ResultExt};
use std::any::Any;
use std::fmt::{self, Write};
use std::ops::{Index, IndexMut, Range};

#[derive(Clone, Eq, PartialEq)]
pub struct Grid<T = char> {
    pub w: usize,
    pub h: usize,
    pub offset_x: isize,
    pub offset_y: isize,
    pub cells: Vec<T>,
}

impl<'s, T: Inpt<'s>> Grid<T> {
    fn from_lines_impl(
        lines: impl IntoIterator<Item = &'s str>,
        trim: CharClass,
        fill: impl Fn() -> Option<T>,
    ) -> InptResult<'s, Self> {
        let mut w = None;
        let mut h = 0;
        let mut cells = Vec::new();
        for line in lines {
            let prev_len = cells.len();
            let mut elements = InptIter::new(line, trim);
            cells.extend(&mut elements);
            elements.outcome?;
            let line_len = cells.len() - prev_len;

            let w = *w.get_or_insert(line_len);
            if w != line_len {
                while cells.len() < prev_len + w {
                    if let Some(x) = fill() {
                        cells.push(x);
                    } else {
                        return Err(InptError::expected::<Self>(line))
                            .message_inside(format_args!(
                                "inconsistent width of {} (expected {})",
                                line_len, prev_len
                            ))
                            .within(line); // TODO: add message
                    }
                }
            }

            h += 1;
        }

        Ok(Grid {
            w: w.unwrap_or(0),
            h,
            offset_x: 0,
            offset_y: 0,
            cells,
        })
    }

    pub fn from_lines(
        lines: impl IntoIterator<Item = &'s str>,
        fill: impl Fn() -> Option<T>,
    ) -> Self {
        Grid::from_lines_impl(lines, WHITESPACE, fill).unwrap()
    }
}

impl Grid<()> {
    pub fn unit(w: usize, h: usize) -> Self {
        Self {
            w,
            h,
            offset_x: 0,
            offset_y: 0,
            cells: vec![(); w * h],
        }
    }
}

impl<T> Grid<T> {
    pub const DIRECTIONS: [(isize, isize); 8] = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ];

    pub fn width(&self) -> isize {
        self.w as isize
    }

    pub fn height(&self) -> isize {
        self.h as isize
    }

    pub fn fill(w: usize, h: usize, mut f: impl FnMut(isize, isize) -> T) -> Self {
        Grid::unit(w, h).map(|x, y, ()| f(x, y))
    }

    pub fn map<V>(mut self, mut f: impl FnMut(isize, isize, T) -> V) -> Grid<V> {
        let x_coords = self.x_coords();
        let y_coords = self.y_coords();
        let mut this = self.cells.drain(..);
        let mut cells = Vec::with_capacity(self.w * self.h);
        for y in y_coords {
            for x in x_coords.clone() {
                cells.push(f(x, y, this.next().unwrap()));
            }
        }

        Grid {
            w: self.w,
            h: self.h,
            offset_x: self.offset_x,
            offset_y: self.offset_y,
            cells,
        }
    }

    pub fn apply<V>(&self, mut f: impl FnMut(isize, isize, &T) -> V) -> Grid<V> {
        let mut this = self.cells.iter();
        let mut cells = Vec::with_capacity(self.w * self.h);
        for y in self.y_coords() {
            for x in self.x_coords() {
                cells.push(f(x, y, this.next().unwrap()));
            }
        }

        Grid {
            w: self.w,
            h: self.h,
            offset_x: self.offset_x,
            offset_y: self.offset_y,
            cells,
        }
    }

    pub fn pad_with(
        &self,
        l: usize,
        r: usize,
        t: usize,
        b: usize,
        f: impl FnMut(isize, isize) -> T,
    ) -> Grid<T>
    where
        T: Clone,
    {
        self.view_with(
            self.offset_x - l as isize,
            self.offset_x + self.w as isize + r as isize,
            self.offset_y - t as isize,
            self.offset_y + self.h as isize + b as isize,
            f,
        )
    }

    pub fn pad(&self, l: usize, r: usize, t: usize, b: usize, value: T) -> Grid<T>
    where
        T: Clone,
    {
        self.pad_with(l, r, t, b, |_, _| value.clone())
    }

    pub fn view_with(
        &self,
        l: isize,
        r: isize,
        t: isize,
        b: isize,
        mut f: impl FnMut(isize, isize) -> T,
    ) -> Grid<T>
    where
        T: Clone,
    {
        let mut new = Grid::fill(
            (r - l).try_into().unwrap_or(0),
            (b - t).try_into().unwrap_or(0),
            |x, y| {
                self.get(x + l, y + t)
                    .cloned()
                    .unwrap_or_else(|| f(x + l, y + t))
            },
        );
        new.offset_x = l;
        new.offset_y = t;
        new
    }

    pub fn view(&self, l: isize, r: isize, t: isize, b: isize, value: T) -> Grid<T>
    where
        T: Clone,
    {
        self.view_with(l, r, t, b, |_, _| value.clone())
    }

    pub fn set_corner(&mut self, x: isize, left: bool, y: isize, top: bool) {
        self.offset_x = if left { x } else { x + self.w as isize };
        self.offset_y = if top { y } else { y + self.h as isize };
    }

    pub fn at(&self, x: isize, y: isize) -> T
    where
        T: Copy,
    {
        self[(x, y)]
    }

    pub fn at_absolute(&self, x: usize, y: usize) -> T
    where
        T: Copy,
    {
        self[(x, y)]
    }

    pub fn get(&self, x: isize, y: isize) -> Option<&T> {
        let x: usize = (x - self.offset_x).try_into().ok()?;
        let y: usize = (y - self.offset_y).try_into().ok()?;
        if x >= self.w || y >= self.h {
            None
        } else {
            Some(&self[(x, y)])
        }
    }

    pub fn get_mut(&mut self, x: isize, y: isize) -> Option<&mut T> {
        let x: usize = (x - self.offset_x).try_into().ok()?;
        let y: usize = (y - self.offset_y).try_into().ok()?;
        if x >= self.w || y >= self.h {
            None
        } else {
            Some(&mut self[(x, y)])
        }
    }

    pub fn set(&mut self, x: isize, y: isize, value: T) {
        if let Some(cell) = self.get_mut(x, y) {
            *cell = value;
        }
    }

    pub fn alter(&mut self, x: isize, y: isize, f: impl FnOnce(&T) -> T) {
        if let Some(cell) = self.get_mut(x, y) {
            *cell = f(cell);
        }
    }

    pub fn neighbors(&self, x: isize, y: isize) -> impl Iterator<Item = &T> + '_ {
        Self::DIRECTIONS
            .iter()
            .filter_map(move |&(dx, dy)| self.get(x + dx, y + dy))
    }

    pub fn coords(&self) -> impl Iterator<Item = (isize, isize)> {
        let xs = self.x_coords();
        let ys = self.y_coords();
        xs.flat_map(move |x| ys.clone().map(move |y| (x, y)))
    }

    pub fn x_coords(&self) -> Range<isize> {
        self.offset_x..(self.offset_x + self.w as isize)
    }

    pub fn y_coords(&self) -> Range<isize> {
        self.offset_y..(self.offset_y + self.h as isize)
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> + '_ {
        self.cells.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> + '_ {
        self.cells.iter_mut()
    }

    pub fn cast(
        &self,
        mut x: isize,
        mut y: isize,
        dx: isize,
        dy: isize,
    ) -> impl Iterator<Item = &T> + '_ {
        std::iter::from_fn(move || {
            let out = self.get(x, y);
            x += dx;
            y += dy;
            out
        })
    }
}

impl<T> Index<(usize, usize)> for Grid<T> {
    type Output = T;

    #[inline]
    fn index(&self, (x, y): (usize, usize)) -> &T {
        assert!(x < self.w);
        assert!(y < self.h);
        &self.cells[x + y * self.w]
    }
}

impl<T> IndexMut<(usize, usize)> for Grid<T> {
    #[inline]
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut T {
        assert!(x < self.w);
        assert!(y < self.h);
        &mut self.cells[x + y * self.w]
    }
}

impl<T> Index<(isize, isize)> for Grid<T> {
    type Output = T;

    #[inline]
    fn index(&self, (x, y): (isize, isize)) -> &T {
        let x = (x - self.offset_x) as usize;
        let y = (y - self.offset_y) as usize;
        &self[(x, y)]
    }
}

impl<T> IndexMut<(isize, isize)> for Grid<T> {
    #[inline]
    fn index_mut(&mut self, (x, y): (isize, isize)) -> &mut T {
        let x = (x - self.offset_x) as usize;
        let y = (y - self.offset_y) as usize;
        &mut self[(x, y)]
    }
}

default impl<'s, T: Inpt<'s>> Inpt<'s> for Grid<T> {
    fn step(
        input: &'s str,
        end: bool,
        trimmed: CharClass,
        guard: &mut RecursionGuard,
    ) -> InptStep<'s, Self> {
        let group = Group::<&'s str>::step(input, end, trimmed, guard);
        InptStep {
            rest: group.rest,
            data: group
                .data
                .and_then(|g| Self::from_lines_impl(g.inner.lines(), trimmed, || None)),
        }
    }
}

impl<'s> Inpt<'s> for Grid<char> {
    fn step(
        input: &'s str,
        end: bool,
        trimmed: CharClass,
        guard: &mut RecursionGuard,
    ) -> InptStep<'s, Self> {
        let group = Group::<&'s str>::step(input, end, trimmed, guard);
        InptStep {
            rest: group.rest,
            data: group
                .data
                .and_then(|g| Self::from_lines_impl(g.inner.lines(), trimmed, || Some(' '))),
        }
    }
}

impl fmt::Display for Grid<char> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h {
            if y != 0 {
                writeln!(f)?;
            }
            for x in 0..self.w {
                write!(f, "{}", &self[(x, y)])?;
            }
        }

        Ok(())
    }
}

default impl<T: fmt::Display> fmt::Display for Grid<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h {
            if y != 0 {
                writeln!(f)?;
            }
            for x in 0..self.w {
                if x != 0 {
                    write!(f, " ")?;
                }
                write!(f, "{}", &self[(x, y)])?;
            }
        }

        Ok(())
    }
}

impl<T: fmt::Debug + Any> fmt::Debug for Grid<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Grid({}..{}, {}..{}) ",
            self.offset_x,
            self.offset_x + self.w as isize,
            self.offset_y,
            self.offset_y + self.h as isize,
        )?;
        let mut l = f.debug_list();
        for y in 0..self.h {
            let mut entry = String::new();
            for x in 0..self.w {
                let v = &self[(x, y)];
                if let Some(c) = (v as &dyn Any).downcast_ref::<char>() {
                    write!(&mut entry, "{}", c).unwrap();
                } else {
                    if !entry.is_empty() {
                        entry.push(' ');
                    }
                    write!(&mut entry, "{:?}", v).unwrap();
                }
            }
            l.entry(&entry);
        }
        l.finish()
    }
}

#[test]
fn test_pad_grid() {
    let checker = Grid::fill(2, 2, |i, j| if (i + j).rem_euclid(2) == 0 { 1 } else { -1 });
    dbg!(&checker);
    assert_eq!(checker.get(-1, -1), None);
    let checker = checker.pad(1, 2, 3, 4, 0);
    dbg!(&checker);

    assert_eq!(checker.at(0, 0), 1);
    assert_eq!(checker.at(0, 1), -1);
    assert_eq!(checker.at(1, 0), -1);
    assert_eq!(checker.at(1, 1), 1);

    assert_eq!(checker.at(-1, -3), 0);
    assert_eq!(checker.get(-2, -3), None);
    assert_eq!(checker.get(-1, -4), None);

    assert_eq!(checker.at(-1, 5), 0);
    assert_eq!(checker.get(-2, 5), None);
    assert_eq!(checker.get(-1, 6), None);

    assert_eq!(checker.at(3, 5), 0);
    assert_eq!(checker.get(4, 5), None);
    assert_eq!(checker.get(3, 6), None);

    assert_eq!(checker.at(3, -3), 0);
    assert_eq!(checker.get(4, -3), None);
    assert_eq!(checker.get(3, -4), None);
}
