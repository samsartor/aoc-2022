use anyhow::{anyhow, Error};
use serde::Deserialize;
use std::process::Command;

#[derive(Deserialize, PartialEq)]
#[serde(rename_all = "lowercase")]
enum TargetKind {
    Bin,
    Lib,
    #[serde(other)]
    Other,
}

#[derive(Deserialize)]
struct Target {
    name: String,
    kind: Vec<TargetKind>,
}

#[derive(Deserialize)]
struct Package {
    name: String,
    targets: Vec<Target>,
}

#[derive(Deserialize)]
struct Metadata {
    packages: Vec<Package>,
}

pub fn list() -> Result<Vec<u32>, Error> {
    let metadata = Command::new("cargo")
        .arg("metadata")
        .arg("--no-deps")
        .arg("--format-version=1")
        .output()?;
    let metadata: Metadata = serde_json::from_slice(&metadata.stdout)?;
    let package = metadata
        .packages
        .into_iter()
        .find(|p| p.name == "aoc")
        .ok_or(anyhow!("no aoc crate found"))?;
    let mut days: Vec<u32> = package
        .targets
        .into_iter()
        .filter(|t| t.kind.contains(&TargetKind::Bin))
        .filter_map(|t| t.name.strip_prefix("day")?.parse().ok())
        .collect();
    days.sort();
    Ok(days)
}
