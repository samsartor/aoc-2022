use anyhow::{bail, Error};
use std::fs;
use std::path::{Path, PathBuf, MAIN_SEPARATOR};

mod days;
mod download;
mod run;

#[derive(clap::Parser)]
#[command(author, version, about, long_about = None)]
/// Download inputs and problems for advent of code 2022.
struct AdventOfCodeCli {
    /// The command to run
    #[clap(subcommand)]
    command: Command,
}

#[derive(clap::Subcommand)]
enum Command {
    /// Download the problem
    #[clap(name = "download", aliases = &["dl", "d"])]
    Download {
        /// The day numbers to download
        days: Vec<u32>,
    },
    #[clap(name = "run", aliases = &["r"])]
    Run {
        /// The day numbers to run
        days: Vec<u32>,
        #[clap(name = "part", short = 'p')]
        part: Option<u32>,
        #[clap(name = "debug", short = 'd')]
        debug: bool,
        #[clap(name = "manual", short = 'm')]
        manual: bool,
        #[clap(name = "example", short = 'e')]
        example: bool,
        #[clap(name = "input", short = 'i')]
        input: Option<PathBuf>,
    },
    #[clap(name = "test")]
    Test {
        /// The day numbers to test
        days: Vec<u32>,
        #[clap(name = "debug", short = 'd')]
        debug: bool,
    },
    #[clap(name = "bench")]
    Bench {
        /// The day numbers to bench
        days: Vec<u32>,
        #[clap(name = "n", short = 'n', default_value = "1000")]
        n: u32,
        #[clap(name = "input", short = 'i')]
        input: Option<PathBuf>,
    },
    #[clap(name = "start")]
    Start {
        /// The days to create
        days: Vec<u32>,
    },
}

fn main() -> Result<(), Error> {
    let AdventOfCodeCli { mut command } = clap::Parser::parse();
    let mut days = match &mut command {
        Command::Download { days, .. }
        | Command::Run { days, .. }
        | Command::Test { days, .. }
        | Command::Start { days, .. }
        | Command::Bench { days, .. } => days.clone(),
    };
    if days.is_empty() {
        days.extend(days::list()?);
    }

    let mut output = Ok(());
    match command {
        Command::Download { .. } => {
            for day in days {
                output = output.and(download::main(day));
            }
        }
        Command::Run {
            part,
            debug,
            manual,
            example,
            input,
            ..
        } => {
            for day in days {
                let (input, out) = match (manual, example, input.clone()) {
                    (true, ..) => (None, false),
                    (_, _, Some(p)) => (Some(p), false),
                    (_, false, None) => (
                        Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into()),
                        true,
                    ),
                    (_, true, None) => (
                        Some(format!("inputs{}day{:02}_example.txt", MAIN_SEPARATOR, day).into()),
                        false,
                    ),
                };
                output = output.and(run::main(
                    day,
                    part,
                    debug,
                    false,
                    out,
                    None,
                    input.as_ref(),
                ));
            }
        }
        Command::Test { debug, .. } => {
            for day in days {
                let input = Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into());
                output = output.and(run::main(
                    day,
                    None,
                    debug,
                    true,
                    false,
                    None,
                    input.as_ref(),
                ));
            }
        }
        Command::Bench { n, mut input, .. } => {
            for day in days {
                if input.is_none() {
                    input = Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into())
                }
                output = output.and(run::main(
                    day,
                    None,
                    false,
                    true,
                    false,
                    Some(n),
                    input.as_ref(),
                ));
            }
        }
        Command::Start { days } => {
            for day in days {
                let path = format!("src/bin/day{day:02}.rs");
                if Path::new(&path).exists() {
                    bail!("{path:?} already exists");
                }
                fs::write(path, include_str!(concat!("day.rs.template")))?;
            }
        }
    };

    output
}
