use aoc::*;

fn part1(input: Grid<char>) -> usize {
    input
        .coords()
        .filter(|&(i, j)| {
            let tree = input.at(i, j);
            [(-1, 0), (1, 0), (0, 1), (0, -1)]
                .into_iter()
                .any(|(di, dj)| input.cast(i, j, di, dj).skip(1).all(|other| *other < tree))
        })
        .count()
}

fn part2(input: Grid<char>) -> usize {
    input
        .coords()
        .map(|(i, j)| {
            let tree = input.at(i, j);
            let score: usize = [(0, -1), (-1, 0), (0, 1), (1, 0)]
                .into_iter()
                .map(|(di, dj)| {
                    let mut score = 0;
                    for &other in input.cast(i, j, di, dj).skip(1) {
                        score += 1;
                        if other >= tree {
                            break;
                        }
                    }
                    score
                })
                .product();
            score
        })
        .max()
        .unwrap()
}

fn main() {
    run(part1);
    run(part2);
}
