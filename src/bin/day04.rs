use aoc::*;

#[derive(Inpt)]
#[inpt(regex = r"(\d+)-(\d+),(\d+)-(\d+)")]
struct Pair(u64, u64, u64, u64);

fn part1(input: Vec<Pair>) -> usize {
    input
        .into_iter()
        .filter(|&Pair(a, b, c, d)| (a..=b).redundant(&(c..=d)))
        .count()
}

fn part2(input: Vec<Pair>) -> usize {
    input
        .into_iter()
        .filter(|&Pair(a, b, c, d)| (a..=b).overlaps(&(c..=d)))
        .count()
}

fn main() {
    run(part1);
    run(part2);
}
