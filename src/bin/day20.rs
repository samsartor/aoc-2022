use aoc::*;

// WRONG: -3646
// WRONG: 6343
// WRONG: 14813
// WRONG: 21557

fn solve(mut input: Vec<i64>, key: i64, times: u64) -> i64 {
    for offset in &mut input {
        *offset *= key;
    }

    let n = input.len() as i64;
    let mut positions: Vec<i64> = (0..n).collect();
    let ordered = |positions: &[i64], rotate: bool| {
        let mut output = vec![i64::MIN; input.len()];
        for (&x, &i) in input.iter().zip(positions) {
            if replace(&mut output[i as usize], x) != i64::MIN {
                panic!("positions are wrong: {i} appears twice in {positions:?}");
            }
        }
        let zero = output.iter().position(|x| *x == 0).unwrap();
        if rotate {
            output.rotate_left(zero);
        }
        #[cfg(feature = "visulize")]
        eprintln!("file: {:?}", output);
        output
    };

    for t in 1..=times {
        #[cfg(feature = "visulize")]
        eprintln!("round: {t}");
        for (i, x) in input.iter().copied().enumerate() {
            ordered(&positions, false);
            let pos0 = positions[i];
            for other in &mut positions {
                if *other >= pos0 {
                    *other -= 1;
                }
            }
            let offset = x % (n - 1);
            let pos1 = (pos0 + offset).rem_euclid(n - 1);
            #[cfg(feature = "visulize")]
            eprintln!("{x} ({offset}): {pos0} -> {pos1}");
            for other in &mut positions {
                if *other >= pos1 {
                    *other += 1;
                }
            }
            positions[i] = pos1;
        }
    }

    let output = ordered(&positions, true);
    [1000, 2000, 3000]
        .into_iter()
        .map(|offset| output[offset % output.len()])
        .sum()
}

fn part1(input: Vec<i64>) -> i64 {
    solve(input, 1, 1)
}

fn part2(input: Vec<i64>) -> i64 {
    solve(input, 811589153, 10)
}

fn main() {
    run(part1);
    run(part2);
}
