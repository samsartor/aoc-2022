use aoc::*;
use std::collections::HashSet;

type Input = HashSet<(i64, i64, i64)>;

fn sides((x, y, z): &(i64, i64, i64)) -> [(i64, i64, i64); 6] {
    [
        (x + 1, y + 0, z + 0),
        (x - 1, y + 0, z + 0),
        (x + 0, y + 1, z + 0),
        (x + 0, y - 1, z + 0),
        (x + 0, y + 0, z + 1),
        (x + 0, y + 0, z - 1),
    ]
}

fn part1(input: Input) -> usize {
    input
        .iter()
        .flat_map(sides)
        .filter(|p| !input.contains(p))
        .count()
}

fn part2(input: Input) -> usize {
    let (mx, my, mz) = input.iter().fold((0, 0, 0), |(mx, my, mz), &(x, y, z)| {
        (mx.max(x), my.max(y), mz.max(z))
    });
    let rx = -1..=mx + 1;
    let ry = -1..=my + 1;
    let rz = -1..=mz + 1;
    let mut queue = vec![(-1, -1, -1)];
    let mut visited = HashSet::new();
    while let Some(p @ (x, y, z)) = queue.pop() {
        if !rx.contains(&x) || !ry.contains(&y) || !rz.contains(&z) {
            continue;
        }
        if input.contains(&p) {
            continue;
        }
        if !visited.insert(p) {
            continue;
        }
        queue.extend(sides(&p));
    }
    let outside = 2 * (mx + 3) * (my + 3) + 2 * (my + 3) * (mz + 3) + 2 * (mx + 3) * (mz + 3);
    part1(visited) - outside as usize
}

fn main() {
    run(part1);
    run(part2);
}
