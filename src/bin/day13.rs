use aoc::*;
use std::cmp::Ordering;

#[derive(Inpt, Debug, Clone, PartialEq, Eq)]
#[inpt(bounds = "", trim = ",")]
enum PacketPart {
    Int(u64),
    Pack(Box<Packet>),
}
use PacketPart::*;

impl Ord for PacketPart {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Int(a), Int(b)) => a.cmp(b),
            (a @ Int(_), b @ Pack(_)) => wrap_part(a.clone()).cmp(b),
            (a @ Pack(_), b @ Int(_)) => a.cmp(&wrap_part(b.clone())),
            (Pack(a), Pack(b)) => a.cmp(b),
        }
    }
}

impl PartialOrd for PacketPart {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Inpt, Debug, Clone, Ord, Eq, PartialEq, PartialOrd)]
struct Packet(#[inpt(split = "Bracketed")] Vec<PacketPart>);

type Input = Vec<(Packet, Packet)>;

fn wrap_pack(x: PacketPart) -> Packet {
    Packet(vec![x])
}

fn wrap_part(x: PacketPart) -> PacketPart {
    PacketPart::Pack(Box::new(wrap_pack(x)))
}

fn part1(input: Input) -> usize {
    input
        .into_iter()
        .enumerate()
        .filter(|(_, (a, b))| a.cmp(b) == Ordering::Less)
        .map(|(i, _)| i + 1)
        .sum()
}

fn part2(mut input: Vec<Packet>) -> usize {
    let a = wrap_pack(wrap_part(PacketPart::Int(2)));
    let b = wrap_pack(wrap_part(PacketPart::Int(6)));
    input.extend([&a, &b].into_iter().cloned());
    input.sort();
    let a_ind = input.iter().position(|x| x == &a).unwrap() + 1;
    let b_ind = input.iter().position(|x| x == &b).unwrap() + 1;
    a_ind * b_ind
}

fn main() {
    run(part1);
    run(part2);
}
