use aoc::*;

#[derive(Inpt)]
#[inpt(regex = r"move (\d+) from (\d) to (\d)")]
struct Move(usize, usize, usize);

#[derive(Inpt)]
#[inpt(trim = "")]
struct Input {
    puzzle: Grid<char>,
    #[inpt(trim = r"\s")]
    moves: Vec<Move>,
}

impl Input {
    fn stacks(&self) -> Vec<Vec<char>> {
        (0..=self.puzzle.w / 4)
            .map(|i| {
                (0..self.puzzle.h - 1)
                    .rev()
                    .map(|j| self.puzzle.at_absolute(i * 4 + 1, j))
                    .filter(|c| !c.is_whitespace())
                    .collect()
            })
            .collect()
    }
}

fn part1(input: Input) -> String {
    let mut stacks = input.stacks();
    for Move(count, from, to) in input.moves {
        for _ in 0..count {
            let crat = stacks[from - 1].pop().unwrap();
            stacks[to - 1].push(crat);
        }
    }
    stacks.iter().map(|stack| stack.last().unwrap()).collect()
}

fn part2(input: Input) -> String {
    let mut stacks = input.stacks();
    let mut tmp = Vec::new();
    for Move(count, from, to) in input.moves {
        for _ in 0..count {
            tmp.push(stacks[from - 1].pop().unwrap());
        }
        tmp.reverse();
        stacks[to - 1].append(&mut tmp);
    }
    stacks.iter().map(|stack| stack.last().unwrap()).collect()
}

fn main() {
    run(part1);
    run(part2);
}
