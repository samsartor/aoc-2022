{ pkgs ? import <nixpkgs> {} }:
with pkgs; mkShell {
  name = "aoc-21";
  
  shellHook =
  ''
    export RUSTUP_TOOLCHAIN="nightly"
    export LIBCLANG_PATH=${pkgs.libclang.lib}/lib
  '';

  packages = [
    rustup
    binutils
    cmake
    clang
    libclang.lib
    lld
    z3
 ];
}