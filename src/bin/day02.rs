use aoc::*;

fn outcome(opp: i32, you: i32) -> i32 {
    match (you - opp).rem_euclid(3) {
        0 => 3,
        1 => 6,
        2 => 0,
        _ => unreachable!(),
    }
}

fn score(opp: i32, you: i32) -> i32 {
    outcome(opp, you) + you + 1
}

fn part1(input: Vec<(char, char)>) -> i32 {
    input
        .iter()
        .map(|&(opp, you)| score(opp as i32 - 'A' as i32, you as i32 - 'X' as i32))
        .sum()
}

fn part2(input: Vec<(char, char)>) -> i32 {
    input
        .iter()
        .map(|&(opp, out)| {
            let opp = opp as i32 - 'A' as i32;
            let out = out as i32 - 'Y' as i32;
            let you = (opp + out).rem_euclid(3);
            score(opp, you)
        })
        .sum()
}

fn main() {
    run(part1);
    run(part2);
}
